var bgcolor;
var button;
var slider;
var nameInput;
var nameP;
let farve = (190);

function setup() {
canvas = createCanvas(500,400);
bgcolor = color(245,118,103);
createP('');

//button

nameInput = createInput('write his/her/its name here');
nameInput.input(updateText);

button = createButton("switch background");
button.mousePressed(changeColor);

createP('');

sliderlabel = createElement('label', 'nice:');
slider = createSlider(10,100,86);
sliderlabel = createElement('label', 'cute:');
slider1 = createSlider(10,100,86);
sliderlabel = createElement('label', 'sweet:');
slider2 = createSlider(10,100,86);
sliderlabel = createElement('label', 'cool:');
slider3 = createSlider(10,100,86);



}

function updateText(){
nameP.html(nameInput.value());
}

function changeColor(){
bgcolor = color(farve,40,random(255));
}


function draw() {

background(bgcolor);
text(nameInput.value(),40,20);


textSize(20);
text('to:', 10, 20);

text('how nice u are:', 40, 60);
fill(252, 180, 214);
ellipse(100,120,slider.value(),slider.value());


text('how cute u are:', 300, 60);
fill(252, 180, 214);
ellipse(370,120,slider1.value(),slider1.value());


text('how sweet u are:', 300, 220);
fill(252, 180, 214);
ellipse(370,280,slider2.value(),slider2.value());

text('how cool u are:', 40, 220);
fill(252, 180, 214);
ellipse(100,280,slider3.value(),slider3.value());

}

function keyPressed(){
  if (keyCode === ENTER){
    saveCanvas("love_letter");
  }
  }
