# **A Generative Program**

<img align="center" src="miniex_7/Pattern.png" width="40%">

**Concept:**

For this week’s mini exercise I decided to program a pattern, which should change colors automatically. I started out with a simple linear pattern, by using a for-loop from the p5.js examples (link below). This worked as the base of my program, and from this, I decided to create three rules to implement: 

1.	Pressing the screen should stop the movement 
2.	Moving the mouse should change the pattern 
3.	A for-loop should make the colors in the pattern switch randomly 

**Technically:**

Adjusting my program to include these rules was not an easy task. I will shortly explain how each rule got implemented in the code: 

Rule 1: In order to stop the movement once the mouse is clicked, I used the syntax “mousePressed” and put the entire draw function into it. This means, that clicking on the screen works as a toggle, switching the draw-function on and off, based on its current state. 

Rule 2: This rule was probably the most difficult part to finish. I worked with a bunch of different rotate-syntax and tried using both cos and sin, to make the vertical lines twist into a more interesting pattern. By using the x and y position of the mouse, you can now control the rotation of the lines and how far they are “bending” (cos/sin). 

Rule 3: I had the for-loop working to draw the lines, and added an if-statement that would control how a line is given a random color, whenever a new line is being drawn. I know this sounds a bit complicated, but it is shown on line 26-31 in my code and looks like this: 

      if (interval%500===0) {
      
     colorchoice[i] = Math.floor(Math.random() * 105) + 100; }

This if-statement is the conditional statement without any direct interactivity, since it makes the colors switch on its own. The math.floor(math.random()) looks a bit scary, but is just code for me to only get random colors within the lighter part of the spectrum. It is for aesthetics and I’m sure there is an easier way to achieve this. 


**Context:**

In class 07 we learned about generative art and how artists have been using different concepts in programming to create automated work. I got inspired by the idea of coding something like a variable image, which is recreating itself again and again. Winnie defined generative art as when the artist hands over control to a system that can function autonomously. This is partly what I did in my mini_ex, but I also wanted the user to experience how they can affect the program. So by being able to change the look of the pattern and pausing it by clicking, you cooperate as a user with the program and create generative art together. 



[RUNME](https://cdn.staticaly.com/gl/ninsway/ap-projekt-/raw/master/miniex_7/miniex_7/index.html)

My inspiration for the code: https://p5js.org/examples/math-random.html
