//rules:
//pressing the screen, pauses the movement
//moving the mouse should change the pattern
//a for-loop should make the colors in the pattern switch randomly


var running = true;
let interval = 0;
let colorchoice = [];

function setup() {
  createCanvas(windowWidth, windowHeight);
  strokeWeight(50);
  frameRate(50);
}

function draw() {

    if (running == true) {
      //using translate moves the patterns position
      translate(width/2, height/2);

      //for every frame, another line is added as long as i < width (until it is filled up)
      for (i = 0; i < width; i++) {

        //a conditional statement without direct interactivity,
        //that controls how many frames it takes for the colors to change
        if (interval%500===0) {

          //an array with the colors that I chose, which looks a bit odd because I only wanted light colors
          colorchoice[i] = Math.floor(Math.random() * 105) + 100;
      }

      //the colors for the lines
      r = color(colorchoice[i], colorchoice[i], colorchoice[i]);

      //choosing the position of the lines
      line(i, 0, i, height);

      //drawing the lines
      stroke(r);

      //rotating the pattern based on the x and y position of the mouse
      //the last part is to slow down how abrupt the pattern changes
      rotate(cos(((mouseX+mouseY)/(width/0.40))));

      //adds 1 to interval for every frame
      //to keep count of the frames for the if-statement
      interval++;
      }
      }  }

//it toggles the code in the first if-statment on and off
function mousePressed() {
    running = !running
}
