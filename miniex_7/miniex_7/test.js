var running = true;
let value = 0;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  strokeWeight(20);
  frameRate(10);
}

function draw() {
    if (running == true) {
        push ();
        translate(width/2, height/2);
            for (let i = 0; i < width; i++) {
                blu = Math.floor(Math.random() * 105) + 150;
                let r = color(random(0), random(0), blu);
                let c = cos(10);
                rotate(c);
                stroke(r);
                line(i, 0, i, height);
            }
        pop();
    }
}

function mousePressed() {
    running = !running
}
