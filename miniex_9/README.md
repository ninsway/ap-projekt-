![Screenshot](miniex_9/Horsens.png)

**Collaboration**

We have collaborated on this miniex_9 as group 2 - CLAN (Cecilie, Laura, Amia, Nina). 
 
**Title**

Need for Speed: Terrific Targets for Traffical Terror
 
**Our program and choice of API**

We chose to use a traffic API from TomTom, which gives us data about traffic behavior, such as speed and density on roads all over the world. Our first thoughts on using this API, was that it could be really interesting to get some data which violates people’s privacy by sharing their current location. This might be data, that could possibly benefit terrorists. We agree in the group, that it is crazy how much information you are able to gather online. We fear how it can be used in cruel and inhumane ways, which one wouldn't even imagine. Here you can pose the question; should this be possible? 

The API we ended up using, shows the current speed on roads all over the world. We chose to focus on highways in some selected areas in Denmark, in order to have data we are able to compare. This mini_ex hereby shows an image of how people are driving on motorways in Denmark in correlation to the legal limit. So in a way, the API can still be used for terror purposes, since fast cars are often on big roads. The faster and the bigger, the easier it would be to ghost-driving and kill a lot of people. 

Our program consists of six different buttons with the names of specific cities, with highways nearby. When a button is pressed the current average speed on the highway of this exact city is shown and moreover, with information about the legal speed-limit at this position. Pressing the button also activates an icon representing a car, which is racing over the screen horizontally. 
We would have liked to link the animated car’s speed, to the actual API, but this was very complex and did not work out. Therefore we gave it an average speed and given that traffic on most motorways are almost the same speed, it works just fine. 
Furthermore, we drew a map of Denmark and marked out the areas of the highways we had chosen. These markings also consist of a interactive element, because you have the option to achieve data from pressing a mark on the map, instead of using the buttons on the left.
The headline of the program captures the whole essence of our idea, which was to address the other side of open source programming and open APIs. Not that it necessarily is our opinion that all data should be private, but this is the point of view we have found a lot in our previous classes, so we wanted to provoke this idea with our program. 
The program also in some way states which highway is the safest in Denmark to be driving on and where are people speeding the most. Whereas, wondering which highway is the most unsafe?
 
**Reflection of progress – acquiring, processing, using and representing data**

We all started by collecting different API’s online that we thought could have some potential of making a great program. This ended up as a long list of different API’s, all from animal-tracking, to Bollywood movies and Pokemons. However we ended up using a traffic tracing API, which could tell us something about traffic flow, traffic incidents and the one that we ended up using: traffic speed. 
 
We chose this API because of the conceptual idea we had. As mentioned before, we wanted a program that provoked something in people. When we all agreed on this term, we just had to find the API that could help create the conceptual idea we wanted. When acquiring the API there were other issues we had to deal with: which of the data should be presented, how and why. 
First we had to understand the data that the API gave us. This was more difficult than expected, because of the different terms used to present the data, were not something we knew before choosing the API. We had some trouble picking the data we wanted to be shown. Our main idea was to get the data of where most cars was at the same time, which turned out to be impossible, which made us settle with the speed of cars instead. This was a compromise, but could still be used to provoke the conceptual idea behind this mini exercise. 
 
**Understanding data and power-relationships in API**

As mentioned before we had trouble understanding the provided data from our API. But after googling and researching on the webpage, where we got the API, we soon understood the data and just had to decide which data to work with for the mini_ex. The data we selected depend on which coordinations we put in. So you only get the data of speed, for the specific area you request. The data that is provided to the user of the API is not very specific data - how many cars are there, at what time is the road the busiest and so on. In this way the API still protects the public, because the most sensitive data is not provided. 
 
It was comforting to see that the more sensitive data, where you get information about the location of most cars etc., is difficult to get. You have to pay for this information and TomTom will trace your use of the API. That way the API providers have the opportunity to prevent the data going into the wrong hands. But even the most basic data can be misused if a person decides to misuse it, - and this subject is the conceptual idea of our exercise. 
The conceptual idea is to question what data should be accessible to everyone - who decides what is sensitive data and who determines what can be used or misused? 
This power relationship is the thing that we are questioning, by trying to showcase a part of the traffic data (speed on highways) and further look into how this can be used. 
 
**Significance of API in digital culture**

API can help the creation of new digital artifacts, but it can also help us understand the digital culture in terms of understanding where information comes from. Other than helping us understand where information comes from, API can reveal the power struggles that one is faced with in the digital culture. Being “new” in the digital culture and wanting to create a new app or platform, there is a lot you don’t know. There is a lot of power struggles, if you want informations for your app or platform, you have to pay for it. API in this digital culture really shows the economic side of the digital culture. 
This is also shown in the reading assignment “API practices and paradigms: Exploring the protocological parameters of APIs as key facilitators of sociotechnical forms of exchange” . In this paper, there is a specific example that shows how difficult it can be to create a new digital artifact on the internet - because of the limitations that are there, caused by the bigger corporations. 
This shows how difficult it can be, being the new one on the block. Trying to fit in and create something new and artistic can be very difficult if you don’t have the money. This shows us that there is not a lot of “place” for new beginners, artistic creations that are made by volunteers and non-profit organizations. 
It also demonstrates how power struggles are not just a thing in corporations, but it is also on digital platforms. 
 
**Ideas for further work**

In relation to our work this week, it would be interesting to go further into how much data you can access via API’s and how all of this data could end up in the wrong hands and end up benefiting really “bad people”. Is API’s something that can benefit terrorists in their cruel intentions, and if this is the case, is it even possible to stop all of this data from coming into the wrong person’s hands?
So how much data are you as a private person even able to access?
And how do we prevent data from ending up in the wrong hands?


Below is the link to a preview of the program, since the API would not work in a link. 

[Miniex_9 Need For Speed](https://www.youtube.com/watch?v=Su8uKset1yo&feature=youtu.be)

