//API key = YDTkTh9i6xTmX88wDQwxYnCig8EohnnE
//Variables completing the url
let traffic;
var api = 'http://api.tomtom.com/traffic/services/4/flowSegmentData/absolute/10/json?';
var apikey = '&key=YDTkTh9i6xTmX88wDQwxYnCig8EohnnE';
var alt = '&point=';
var coordinateslol = ['56.20011220324803,10.076024883548598','55.74064134,12.45660782','55.87791088,9.77989197','55.36184416,10.33779144','57.015131857131166,9.950695037841799','54.723033993884144,11.434020996093752'];
var unit = '&unit=kmph';
var url;
var button;

//images
let den;
let imgpew;

//Variables regarding the car (movement, colours)
var speed = 0;
let y=510;
let col={
  r:100,
  g:100,
  b:130
}

//Variables for buttons
var aarhusknap;
var horsensknap;
var copenhagenknap;
var odenseknap;
var aalborgknap;
var lollandknap;

//coordinates for the chosen positions
//aarhus 56.20011220324803,10.076024883548598
//horsens 55.87791088, 9.77989197
//copenhagen 55.74064134,12.45660782
//odense 55.36184416,10.33779144
//aalborg 57.015131857131166,9.950695037841799
//lolland 54.723033993884144,11.434020996093752

function setup(){
  frameRate(10);
  createCanvas(windowWidth,650);
  background(100,200,255);

//image of denmark-map
  push ();
  den=createImg('DenmarkMap.png');
  den.size(520,420);
  den.position(520,20);
  pop();

//the next section shows the code for the buttons, first the buttons on
//the left side of the canvas, and then for the images on the map.

  //Aarhus button on the left side
  aarhusknap = createButton('Aarhus')
  aarhusknap.position(50,300);
  aarhusknap.mousePressed(nyAarhus);
  aarhusknap.style("font-family","American Typewriter");
  aarhusknap.style("background-color","#0B2161");
  aarhusknap.style("color","#fff");
  aarhusknap.size(180,30);
  aarhusknap.style('font-size',"18px");

  //Button for aarhus on the map
  button=createImg('pewpew.png');
  button.size(30,30);
  button.position(650,200);
  button.mousePressed(nyAarhus);

  //Horsens button on the left side
  horsensknap = createButton('Horsens');
  horsensknap.position(250,300);
  horsensknap.mousePressed(nyHorsens);
  horsensknap.style("font-family","American Typewriter");
  horsensknap.style("background-color","#0B2161");
  horsensknap.style("color","#fff");
  horsensknap.size(180,30);
  horsensknap.style('font-size',"18px");

  //Button for horsens on the map
  button=createImg('pewpew.png');
  button.size(30,30);
  button.position(630,244);
  button.mousePressed(nyHorsens);

  //Copenhagen button on the left side
  copenhagenknap = createButton('Copenhagen');
  copenhagenknap.position(250,350);
  copenhagenknap.mousePressed(nyCopenhagen);
  copenhagenknap.style("font-family","American Typewriter");
  copenhagenknap.style("background-color","#0B2161");
  copenhagenknap.style("color","#fff");
  copenhagenknap.size(180,30);
  copenhagenknap.style('font-size',"18px");

  //Button for copenhagen on the map
  button=createImg('pewpew.png');
  button.size(30,30);
  button.position(810,270);
  button.mousePressed(nyCopenhagen);

  //Odense button on the left side
  odenseknap = createButton('Odense');
  odenseknap.position(50,350);
  odenseknap.mousePressed(nyOdense);
  odenseknap.style("font-family","American Typewriter");
  odenseknap.style("background-color","#0B2161");
  odenseknap.style("color","#fff");
  odenseknap.size(180,30);
  odenseknap.style('font-size',"18px");

  //Button for odense on the map
  button=createImg('pewpew.png');
  button.size(30,30);
  button.position(658,305);
  button.mousePressed(nyOdense);

  //Aalborg button on the left side
  aalborgknap = createButton('Aalborg')
  aalborgknap.position(50,400);
  aalborgknap.mousePressed(nyAalborg);
  aalborgknap.style("font-family","American Typewriter");
  aalborgknap.style("background-color","#0B2161");
  aalborgknap.style("color","#fff");
  aalborgknap.size(180,30);
  aalborgknap.style('font-size',"18px");

  //Button for aalborg on the map
  button=createImg('pewpew.png');
  button.size(30,30);
  button.position(635,80);
  button.mousePressed(nyAalborg);

  //Lolland button on the left side
  lollandknap = createButton('Lolland')
  lollandknap.position(250,400);
  lollandknap.mousePressed(nyLolland);
  lollandknap.style("font-family","American Typewriter");
  lollandknap.style("background-color","#0B2161");
  lollandknap.style("color","#fff");
  lollandknap.size(180,30);
  lollandknap.style('font-size',"18px");

  //Button for lolland on the map
  button=createImg('pewpew.png');
  button.size(30,30);
  button.position(735,391);
  button.mousePressed(nyLolland);
  }

function draw(){
  //Titel
  push();
  textFont('Phosphate');
  fill(50,50,100);
  textSize(60);
  text('Need For Speed', 50, 100);
  textSize(20);
  text('Danish highways', 300, 50);
  textFont('Helvetica');
  textSize(28);
  text('Terrific Targets for Traffical Terror',50,150);
  pop();

  //Road
  fill(100);
  rect(0,450,width,150);
  let x=0;
  while(x<=width){
    fill(255);
    rect(x,525,20,7);
    x=x+100;
  }

  //Car
  fill(col.r,col.g,col.b);
  rect(speed,y,55,25,5,5);
  speed+=6;
  if(speed>=width){
    y=random(450,500);
    speed=0;
    col.g=random(100,255);
    col.b=random(100,255);
  }
}
//Coordinates for our road in Aarhus
function nyAarhus(){
  url = api+apikey+alt+coordinateslol[0]+unit;
  loadJSON(url, displayAarhus);
}

function displayAarhus(traffic){
  //Collecting the data
  if (traffic) {
    //Rect covering text :)
    fill(100,200,255);
    rect(0,0,width,height);
    //Text collecting the current speed
    fill(0);
    textSize(20);
    textFont('American Typewriter');
    text('Aarhus:',50,200)
    text('Current speed at position:  '+traffic.flowSegmentData.currentSpeed+'  KMpH',50,230);
    text('Legal speedlimit at position: 120 KMpH',50,260);
    //Car starting over / New car
    speed=0;
    y=random(450,550);
    col.g=random(0,255);
    col.b=random(0,255);
  }
}

//Coordinates for our road in Horsens
function nyHorsens(){
  url = api+apikey+alt+coordinateslol[1]+unit;
  loadJSON(url, displayHorsens);
}

function displayHorsens(traffic){
  if (traffic) {
    fill(100,200,255);
    rect(0,0,width,height);
    fill(0);
    textSize(20);
    textFont('American Typewriter');
    text('Horsens:',50,200)
    text('Current speed at position:  '+traffic.flowSegmentData.currentSpeed+'  KMpH',50,230);
    text('Legal speedlimit at position: 130 KMpH',50,260);
    speed=0;
    y=random(450,550);
    col.g=random(0,200);
    col.b=random(0,200);
  }
}

//Coordinates for our road in Copenhagen
function nyCopenhagen(){
  url = api+apikey+alt+coordinateslol[2]+unit;
  loadJSON(url, displayCopenhagen);
}

function displayCopenhagen(traffic){
  if (traffic) {
    fill(100,200,255);
    rect(0,0,width,height);
    fill(0);
    textSize(20);
    textFont('American Typewriter');
    text('Copenhagen:',50,200)
    text('Current speed at position:  '+traffic.flowSegmentData.currentSpeed+'  KMpH',50,230);
    text('Legal speedlimit at position: 110 KMpH',50,260);
    speed=0;
    y=random(450,550);
    col.g=random(0,100);
    col.b=random(0,100);
  }
  }

//Coordinates for our road outside of Odense
function nyOdense(){
  url = api+apikey+alt+coordinateslol[3]+unit;
  loadJSON(url, displayOdense);
}

function displayOdense(traffic){
  if (traffic) {
    fill(100,200,255);
    rect(0,0,width,height);
    fill(0);
    textSize(20);
    textFont('American Typewriter');
    text('Odense:',50,200)
    text('Current speed at position:  '+traffic.flowSegmentData.currentSpeed+'  KMpH',50,230);
    text('Legal speedlimit at position: 120 KMpH',50,260);
    speed=0;
    y=random(450,550);
    col.g=random(0,100);
    col.b=random(0,100);
  }
}

//Coordinates for our road outside of Aalborg
function nyAalborg(){
  url = api+apikey+alt+coordinateslol[4]+unit;
  loadJSON(url, displayAalborg);
}

function displayAalborg(traffic){
  if (traffic) {
    fill(100,200,255);
    rect(0,0,width,height);
    fill(0);
    textSize(20);
    textFont('American Typewriter');
    text('Aalborg:',50,200)
    text('Current speed at position:  '+traffic.flowSegmentData.currentSpeed+'  KMpH',50,230);
    text('Legal speedlimit at position: 130 KMpH',50,260);
    speed=0;
    y=random(450,550);
    col.g=random(0,100);
    col.b=random(0,100);
  }
}

//Coordinates for our road outside of Lolland
function nyLolland(){
  url = api+apikey+alt+coordinateslol[5]+unit;
  loadJSON(url, displayLolland);
}

function displayLolland(traffic){
  if (traffic) {
    fill(100,200,255);
    rect(0,0,width,height);
    fill(0);
    textSize(20);
    textFont('American Typewriter');
    text('Lolland:',50,200)
    text('Current speed at position:  '+traffic.flowSegmentData.currentSpeed+'  KMpH',50,230);
    text('Legal speedlimit at position: 120 KMpH',50,260);
    speed=0;
    y=random(450,550);
    col.g=random(0,100);
    col.b=random(0,100);
  }
}
