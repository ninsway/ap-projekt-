
let angle = 0;
let check=0;
var stars = [];


function setup() {
createCanvas(800, 580);
angleMode(DEGREES);
background(29,51,84);
frameRate(50);

//star
for (var i = 0; i < 50; i++) {
    stars.push(new Star());
}

}


function draw() {

  fill(29,51,84);
  rect(0,0,800,580)

  angle = angle + 0.3;

//background changing color
  if(frameCount%340==0===true){
    check=1;
  }

  if(frameCount%850==0===true){
    check=0;
  }

//daytime
  if(check===1){
    fill(59,212,232);
    rect(0,0,800,580)
    fill(255,255,255,191)
//clouds
ellipse(250,400,50);
ellipse(280,400,50);
ellipse(270,380,50);
ellipse(300,400,50);
ellipse(320,400,50);
ellipse(290,380,50);

ellipse(550,100,50);
ellipse(580,100,50);
ellipse(570,80,50);
ellipse(600,100,50);
ellipse(620,100,50);
ellipse(590,80,50);

ellipse(150,200,50);
ellipse(180,200,50);
ellipse(170,180,50);
ellipse(200,200,50);
ellipse(220,200,50);
ellipse(190,180,50);

ellipse(665,400,20);
ellipse(669,400,20);
ellipse(675,390,20);
ellipse(680,400,20);
ellipse(690,400,20);
ellipse(690,390,20);

//night
  }else if(check===0){
    fill(29,51,84);
    rect(0,0,800,580)
//stars
    for (var i = 0; i < 50; i++) {
        stars[i].draw();
    } }

  push ();
  translate(400,600);

//solen
  fill(255,253,52);
  noStroke ();
  rotate(angle);
  ellipse(0,400,100);

//månen
  fill(253,252,226);
  noStroke ();
  ellipse(0,-400,100);

  //midten
  fill(29,51,84);
  ellipse(0,0,20);

  pop ();

  //text
  textSize(32);
  text('loading all day and night...', 220, 30);


}

//stjerner
function Star() {
   this.x = random(windowWidth);
   this.y = random(windowHeight-200);
}

Star.prototype.draw = function() {
  noStroke();
  fill(255, 255, 0);
  ellipse(this.x, this.y, 2, 2);
}
