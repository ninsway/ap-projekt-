let lead;
let back;
let profile;
let img1;
let img2;
let img3;
let img4;
let img5;
let img6;
let img7;
let img8;
let img9;
let logo1;
let logo2;
let logo3;
let logo4;
let logo5;
let logo6;
let logo7;
let weblinks;

function preload() {
  //load profile picture
  profile=loadImage('Avatars/AvatarProfile.png');
  sora=loadImage('Avatars/SORA.png');

  //Load Avatars
  img1= loadImage('Avatars/Person1.png');//Laura
  img2= loadImage('Avatars/Person2.png');//Nina
  img3= loadImage('Avatars/Person3.png');//Cecilie
  img4= loadImage('Avatars/Person4.png');//Amia
  img6= loadImage('Avatars/Person6.png');//Kim
  img7= loadImage('Avatars/Person7.png');//Chili
  img8= loadImage('Avatars/Person8.png');//Ian
  img9= loadImage('Avatars/Person9.png');//Mira
  img5= loadImage('Avatars/Person5.png');//Alan

  //Load Logos
  logo1= loadImage('Logos/Facebook-logo.png');
  logo2= loadImage('Logos/Instagram-logo.png');
  logo3= loadImage('Logos/Snapchat-logo.png');
  logo4= loadImage('Logos/Spotify-logo.png');
  logo5= loadImage('Logos/Tinder-logo.png');
  logo6= loadImage('Logos/Twitter-logo.png');
  logo7= loadImage('Logos/YouTube-logo.png');

  //Array for links to different social media
  weblinks=['https://www.facebook.com/','https://www.instagram.com/','https://www.snapchat.com/l/da-dk/','https://twitter.com/','https://tinder.com/','https://www.youtube.com/','https://open.spotify.com/browse/featured']
}

function setup(){
  createCanvas(1400,770);
  background(153, 204, 255);

  image(sora,665,100,150,500);

  //Button for going back
  back=createButton('🔙');
  back.position(110,660);
  back.style('padding','2px 70px');
  back.style('background','#0086b3');
  back.style('font-size','25px');
  back.style('color', 'white');
  back.mousePressed(goback)
  back.hide();

  //the square around the profile
  fill(254, 199, 255);
  stroke(0);
  rect(200,100,450,490,20);

  //Current rating box
  fill(254, 199, 255);
  stroke(0);
  rect(200,40,1050,50,20);
  fill(175, 97, 176);
  textSize(30);
  textStyle(BOLD);
  text('C  U  R  R  E  N  T     R  A  T  I  N  G:    🕸️ 5 0 ',420,75);

  //social media boxes
  fill(254, 199, 255);
  stroke(0);
  rect(800,100,450,70,20);
  rect(800,170,450,70,20);
  rect(800,240,450,70,20);
  rect(800,310,450,70,20);
  rect(800,380,450,70,20);
  rect(800,450,450,70,20);
  rect(800,520,450,70,20);

  //square around the profile picture
  fill(255);
  noStroke();
  rect(325,120,200,200,20);

  //Show profile picture
  image(profile, 340, 130, 170, 170);

  //Button for leaderboard
  lead=createButton('L E A D E R B O A R D');
  lead.position(230,600);
  lead.mousePressed(leaderboard);
  lead.size(180,50);
  lead.style('background','#FE94FF');
  lead.style('font-size', '13px');
  lead.style('color', 'black');

  //text in profile
  fill(175, 97, 176);
  textSize(15);
  noStroke();
  text('Y O U R   P R O F I L E',345,340)
  fill(0);
  text('NAME:',250,370);
  text('AGE:',250,400);
  text('OCCUPATION:',250,430);
  text('TIME ONLINE:',250,460);
  text('STATUS:',250,490);
  text('MOTTO:',250,520);
  textStyle(NORMAL);
  noStroke();
  text('Nanna Hansen',380,370);
  text('23',380,400);
  text('Studing at Oxford University',380,430);
  text('4 hours', 380,460);
  text('In a relationship with Ian',380,490);
  text('"Live life bold"',380,520);

  //logos
  image(logo1, 825, 115, 35, 35);
  image(logo2, 825, 185, 38, 38);
  image(logo3, 822, 255, 42, 40);
  image(logo4, 820, 530, 50, 50);
  image(logo5, 822, 398, 35, 35);
  image(logo6, 822, 325, 40, 40);
  image(logo7, 820, 462, 50, 50);

  //Text in social media boxes
  fill(0);
  textSize(25);
  text('Facebook',880,145);
  text('Instagram',880,215);
  text('Snapchat',880,285);
  text('Twitter', 880,355);
  text('Tinder', 880,425);
  text('Youtube', 880,495);
  text('Spotify',880,565);

  //Text for points
  text('🕸️ 13',1150,145);
  text('🕸️ 10',1150,215);
  text('🕸️  5',1150,285);
  text('🕸️  4', 1150,355);
  text('🕸️  5', 1150,425);
  text('🕸️ 10', 1150,495);
  text('🕸️  3',1150,565);

  //Direct to Facebook
  fbbut=createButton(' ');
  fbbut.style('font-size','24px');
  fbbut.position(820,110);
  fbbut.style('border','none');
  fbbut.style('background-color','#00E6AC00');
  fbbut.style('padding','20px 200px')
  fbbut.mousePressed(face);
  fbbut.style('cursor','pointer')

  //Direct to Instagram
  insbut=createButton(' ');
  insbut.style('font-size','24px');
  insbut.position(820,195);
  insbut.style('border','none');
  insbut.style('background-color','#00E6AC00');
  insbut.style('padding','20px 200px')
  insbut.mousePressed(insta);
  insbut.style('cursor','pointer')

  //Direct to Snapchat
  snabut=createButton(' ');
  snabut.style('font-size','24px');
  snabut.position(820,265);
  snabut.style('border','none');
  snabut.style('background-color','#00E6AC00');
  snabut.style('padding','20px 200px')
  snabut.mousePressed(snap);
  snabut.style('cursor','pointer')

  //Direct to Twitter
  twibut=createButton(' ');
  twibut.style('font-size','24px');
  twibut.position(820,320);
  twibut.style('border','none');
  twibut.style('background-color','#00E6AC00');
  twibut.style('padding','20px 200px')
  twibut.mousePressed(twitter);
  twibut.style('cursor','pointer')

  //Direct to Tinder
  tinbut=createButton(' ');
  tinbut.style('font-size','24px');
  tinbut.position(820,390);
  tinbut.style('border','none');
  tinbut.style('background-color','#00E6AC00');
  tinbut.style('padding','20px 200px')
  tinbut.mousePressed(tinder);
  tinbut.style('cursor','pointer')

  //Direct to YouTube
  youbut=createButton(' ');
  youbut.style('font-size','24px');
  youbut.position(820,460);
  youbut.style('border','none');
  youbut.style('background-color','#00E6AC00');
  youbut.style('padding','20px 200px')
  youbut.mousePressed(youtube);
  youbut.style('cursor','pointer')

  //Direct to Spotify
  spobut=createButton(' ');
  spobut.style('font-size','24px');
  spobut.position(820,530);
  spobut.style('border','none');
  spobut.style('background-color','#00E6AC00');
  spobut.style('padding','20px 200px')
  spobut.mousePressed(spotify);
  spobut.style('cursor','pointer')
}

//When button is pressed
function leaderboard(){

  //Show Back-button
  back.show();

  //"Background"
  fill(153, 204, 255);
  stroke(0);
  rect(0,0,width,height);
  fill(255);

  image(sora,885,160,150,500);

  //Profile pictures
  image(img1, 115, 205, 37, 37);//Laura
  image(img2, 115, 255, 37, 37);//Nina
  image(img3, 115, 305, 37, 37);//Cecilie
  image(img4, 115, 355, 37, 37);//Amia
  image(img6, 115, 405, 37, 37);//Kim
  image(img7, 115, 455, 37, 37);//Chili
  image(profile, 115, 505, 37, 37);//Nanna
  image(img9, 115, 555, 37, 37);//Mira
  image(img5, 115, 605, 37, 37);//Alan

  //rect with the dark blue color
  push ();
  fill(0, 134, 179);
  stroke(0);
  rect(100,150,750,50,20);
  pop ();

  //leaderboard rects
  noFill();
  stroke(0);
  rect(100,150,750,100,20); //the square around the profile
  rect(100,150,750,150,20); //the square around the profile
  rect(100,150,750,200,20); //the square around the profile
  rect(100,150,750,250,20); //the square around the profile
  rect(100,150,750,300,20); //the square around the profile
  rect(100,150,750,350,20); //the square around the profile
  rect(100,150,750,400,20); //the square around the profile
  rect(100,150,750,450,20); //the square around the profile
  rect(100,150,750,500,20); //the square around the profile
  fill(0);

  //Level overview
  push ();
  fill(0, 134, 179);
  stroke(0);
  rect(1050,150,300,50,20);
  pop ();

  noFill();
  stroke(108, 107, 108);
  rect(1050,150,300,100,20); //the square around the profile
  rect(1050,150,300,150,20); //the square around the profile
  rect(1050,150,300,200,20); //the square around the profile
  rect(1050,150,300,250,20); //the square around the profile
  rect(1050,150,300,300,20); //the square around the profile
  rect(1050,150,300,350,20); //the square around the profile
  rect(1050,150,300,400,20); //the square around the profile
  rect(1050,150,300,450,20); //the square around the profile
  rect(1050,150,300,500,20); //the square around the profile
  fill(0);

  fill(0);
  noStroke();
  textSize(25);

  //text in the profiles
  noStroke();
  text('Laura Devantié', 180,230);
  text('Nina Galsgaard',180,280);
  text('Cecilie Christensen',180,330);
  text('Amia Nielsen',180,380);
  text('Kim Svenson',180,430);
  text('Chili Young',180,480);
  text('Nanna Hansen',180,530);
  text('Mira Rasmussen',180,580);
  text('Alan Johansen',180,630);

  //text at the top bar of leaderboard
  fill(254, 199, 255);
  text('Friends',120,180);
  text('Webs',500,180);
  text('Level',650,180);

  //text at the top bar of overview
  fill(254, 199, 255);
  text('Level overview',1110,180);


  //Text for top
  push ();
  textSize(50);
  fill(175, 97, 176);
  textStyle(BOLD);
  text('L E A D E R B O A R D', 100,100);
  pop ();

  //Level overview: names
  fill(108, 107, 108   );
  noStroke();
  text('God',1230,230);
  text('Legend',1230,280);
  text('Hero',1230,330);
  text('Knight',1230,380);
  text('Slayer',1230,430);
  text('Scout',1230,480);
  text('Fighter',1230,530);
  text('Novice',1230,580);
  text('Beginner',1230,630);

  //Level overview: ranks
  fill(108, 107, 108);
  noStroke();
  text('🕸️ 90-100',1065,230);
  text('🕸️ 80-89',1065,280);
  text('🕸️ 70-79',1065,330);
  text('🕸️ 60-69',1065,380);
  text('🕸️ 50-59',1065,430);
  text('🕸️ 40-49',1065,480);
  text('🕸️ 30-39',1065,530);
  text('🕸️ 20-29',1065,580);
  text('🕸️ 10-19',1065,630);


  //Points
  fill(0);
  noStroke();
  text('🕸️ 80',500,230);//Lauras Points
  text('🕸️ 77',500,280);//Ninas Points
  text('🕸️ 75',500,330);//Cecilies Points
  text('🕸️ 69',500,380);//Amias Points
  text('🕸️ 57',500,430);//Kims Points
  text('🕸️ 55',500,480);//Chilis Points
  text('🕸️ 50',500,530);//Ians Points
  text('🕸️ 43',500,580);//Miras Points
  text('🕸️ 42',500,630);//Alans Points

  //Levels
  fill(0);
  noStroke();
  text('Legend',650,230);//Lauras Level
  text('Hero',650,280);//Ninas Level
  text('Hero',650,330);//Cecilies Level
  text('Knight',650,380);//Amias Level
  text('Slayer',650,430);//Kims Level
  text('Slayer',650,480);//Chilis Level
  text('Slayer',650,530);//Ians Level
  text('Scout',650,580);//Miras Level
  text('Scout',650,630);//Alans Level


  //Strokes between categories
  stroke(0);
  strokeWeight(1);
  beginShape(LINES);
  vertex(450, 151);
  vertex(450, 650);
  vertex(630, 151);
  vertex(630, 650);
  endShape();

  //Stroke level overview
  stroke(108, 107, 108);
  strokeWeight(1);
  beginShape(LINES);
  vertex(1200, 200);
  vertex(1200, 650);
  endShape();

  //Circle for ranking
  noStroke();
  fill(230, 184, 0);
  circle(80,210,15);
  fill(217, 217, 217);
  circle(80,265,15);
  fill(204, 122, 0);
  circle(80,320,15);

  //number for ranking
  fill(0);
  noStroke();
  text('1',73,217);
  text('2',73,272);
  text('3',73,327);
  text('4',73,372);
  text('5',73,420);
  text('6',73,472);
  text('7',73,525);
  text('8',73,575);
  text('9',73,630);

  //Hide leader-button
  lead.hide();
}

//Open facebook
function face(){
  window.open(weblinks[0]);
}

//open instagram
function insta(){
  window.open(weblinks[1]);
}

//open snapchat
function snap(){
  window.open(weblinks[2]);
}

//open snapchat
function twitter(){
  window.open(weblinks[3]);
}

//open tinder
function tinder(){
  window.open(weblinks[4]);
}

//open youtube
function youtube(){
  window.open(weblinks[5]);
}

//open spotify
function spotify(){
  window.open(weblinks[5]);
}

//Go to main page again
function goback(){

  //Hide back-button
  back.hide();

  //"Background"
  fill(153, 204, 255);
  stroke(0);
  rect(0,0,width,height);

  image(sora,665,100,150,500);

  //the square around the profile
  fill(254, 199, 255);
  stroke(0);
  rect(200,100,450,490,20);

  //Current rating box
  fill(254, 199, 255);
  stroke(0);
  rect(200,40,1050,50,20);
  fill(175, 97, 176);
  textSize(30);
  textStyle(BOLD);
  text('C  U  R  R  E  N  T     R  A  T  I  N  G:    🕸️ 5 0 ',420,75);

  //social media boxes
  fill(254, 199, 255);
  stroke(0);
  rect(800,100,450,70,20);
  rect(800,170,450,70,20);
  rect(800,240,450,70,20);
  rect(800,310,450,70,20);
  rect(800,380,450,70,20);
  rect(800,450,450,70,20);
  rect(800,520,450,70,20);

  //square around the profile picture
  fill(255);
  stroke(0);
  rect(325,120,200,200,20);

  //Show profile picture
  image(profile, 340, 130, 170, 170);

  //Button for leaderboard
  lead=createButton('L E A D E R B O A R D');
  lead.position(230,600);
  lead.mousePressed(leaderboard);
  lead.size(180,50);
  lead.style('background','#FE94FF');
  lead.style('font-size', '13px');
  lead.style('color', 'black');

  //profile
  noFill();
  stroke(0);
  rect(200,100,450,490,20); //the square around the profile

  //text in profile
  fill(175, 97, 176);
  textSize(15);
  noStroke();
  text('Y O U R   P R O F I L E',345,340)
  fill(0);
  text('NAME:',250,370);
  text('AGE:',250,400);
  text('OCCUPATION:',250,430);
  text('TIME ONLINE:',250,460);
  text('STATUS:',250,490);
  text('MOTTO:',250,520);
  textStyle(NORMAL);
  noStroke();
  text('Nanna Hansen',380,370);
  text('23',380,400);
  text('Studing at Oxford University',380,430);
  text('4 hours', 380,460);
  text('In a relationship with Ian',380,490);
  text('"Live life bold"',380,520);

  //logos
  image(logo1, 825, 115, 35, 35);
  image(logo2, 825, 185, 38, 38);
  image(logo3, 822, 255, 42, 40);
  image(logo4, 820, 530, 50, 50);
  image(logo5, 822, 398, 35, 35);
  image(logo6, 822, 325, 40, 40);
  image(logo7, 820, 462, 50, 50);

  //Text in social media boxes
  fill(0);
  noStroke();
  textSize(25);
  text('Facebook',880,145);
  text('Instagram',880,215);
  text('Snapchat',880,285);
  text('Twitter', 880,355);
  text('Tinder', 880,425);
  text('Youtube', 880,495);
  text('Spotify',880,565);

  //Text for points
  text('🕸️ 13',1150,145);
  text('🕸️ 10',1150,215);
  text('🕸️  5',1150,285);
  text('🕸️  4', 1150,355);
  text('🕸️  5', 1150,425);
  text('🕸️ 10', 1150,495);
  text('🕸️  3',1150,565);

  //Direct FB
  fbbut=createButton(' ');
  fbbut.style('font-size','24px');
  fbbut.position(820,110);
  fbbut.style('border','none');
  fbbut.style('background-color','#00E6AC00');
  fbbut.style('padding','20px 200px')
  fbbut.mousePressed(face);
  fbbut.style('cursor','pointer')

  //Direct insta
  insbut=createButton(' ');
  insbut.style('font-size','24px');
  insbut.position(820,195);
  insbut.style('border','none');
  insbut.style('background-color','#00E6AC00');
  insbut.style('padding','20px 200px')
  insbut.mousePressed(insta);
  insbut.style('cursor','pointer')

  //Direct snap
  snabut=createButton(' ');
  snabut.style('font-size','24px');
  snabut.position(820,265);
  snabut.style('border','none');
  snabut.style('background-color','#00E6AC00');
  snabut.style('padding','20px 200px')
  snabut.mousePressed(snap);
  snabut.style('cursor','pointer')

  //direct Twitter
  twibut=createButton(' ');
  twibut.style('font-size','24px');
  twibut.position(820,320);
  twibut.style('border','none');
  twibut.style('background-color','#00E6AC00');
  twibut.style('padding','20px 200px')
  twibut.mousePressed(twitter);
  twibut.style('cursor','pointer')

  //Direct Tinder
  tinbut=createButton(' ');
  tinbut.style('font-size','24px');
  tinbut.position(820,390);
  tinbut.style('border','none');
  tinbut.style('background-color','#00E6AC00');
  tinbut.style('padding','20px 200px')
  tinbut.mousePressed(tinder);
  tinbut.style('cursor','pointer')

  //Direct youtube
  youbut=createButton(' ');
  youbut.style('font-size','24px');
  youbut.position(820,460);
  youbut.style('border','none');
  youbut.style('background-color','#00E6AC00');
  youbut.style('padding','20px 200px')
  youbut.mousePressed(youtube);
  youbut.style('cursor','pointer')

  //Direct spotify
  spobut=createButton(' ');
  spobut.style('font-size','24px');
  spobut.position(820,530);
  spobut.style('border','none');
  spobut.style('background-color','#00E6AC00');
  spobut.style('padding','20px 200px')
  spobut.mousePressed(spotify);
  spobut.style('cursor','pointer')
}
