let img1;
let img2;
let img3;
let img4;
let img5;
let img6;
let img7;
let img8;
let img9;

function preload() {

  profile=loadImage('Avatars/AvatarProfile.png');

  //Load Avatars
  img1= loadImage('Avatars/Person1.png');//Laura
  img2= loadImage('Avatars/Person2.png');//Nina
  img3= loadImage('Avatars/Person3.png');//Cecilie
  img4= loadImage('Avatars/Person4.png');//Amia
  img6= loadImage('Avatars/Person6.png');//Kim
  img7= loadImage('Avatars/Person7.png');//Chili
  img8= loadImage('Avatars/Person8.png');//Ian
  img9= loadImage('Avatars/Person9.png');//Mira
  img5= loadImage('Avatars/Person5.png');//Alan

  //Load L/da-dk/','https://twitter.com/','https://tinder.com/','https://www.youtube.com/','https://open.spotify.com/browse/featured']
}

function setup(){
  createCanvas(1400,770);
  background(153, 204, 255);

//"Background"
fill(153, 204, 255);
stroke(0);
rect(0,0,width,height);
fill(255);

//Profile pictures
image(img1, 115, 205, 37, 37);//Laura
image(img2, 115, 255, 37, 37);//Nina
image(img3, 115, 305, 37, 37);//Cecilie
image(img4, 115, 355, 37, 37);//Amia
image(img6, 115, 405, 37, 37);//Kim
image(img7, 115, 455, 37, 37);//Chili
image(profile, 115, 505, 37, 37);//Nanna
image(img9, 115, 555, 37, 37);//Mira
image(img5, 115, 605, 37, 37);//Alan

//rect with the dark blue color
push ();
fill(0, 134, 179);
stroke(0);
rect(100,150,750,50,20);
pop ();

//leaderboard rects
noFill();
stroke(0);
rect(100,150,750,100,20); //the square around the profile
rect(100,150,750,150,20); //the square around the profile
rect(100,150,750,200,20); //the square around the profile
rect(100,150,750,250,20); //the square around the profile
rect(100,150,750,300,20); //the square around the profile
rect(100,150,750,350,20); //the square around the profile
rect(100,150,750,400,20); //the square around the profile
rect(100,150,750,450,20); //the square around the profile
rect(100,150,750,500,20); //the square around the profile
fill(0);

//Level overview
push ();
fill(0, 134, 179);
stroke(0);
rect(950,150,300,50,20);
pop ();

noFill();
stroke(108, 107, 108);
rect(950,150,300,100,20); //the square around the profile
rect(950,150,300,150,20); //the square around the profile
rect(950,150,300,200,20); //the square around the profile
rect(950,150,300,250,20); //the square around the profile
rect(950,150,300,300,20); //the square around the profile
rect(950,150,300,350,20); //the square around the profile
rect(950,150,300,400,20); //the square around the profile
rect(950,150,300,450,20); //the square around the profile
rect(950,150,300,500,20); //the square around the profile
fill(0);

fill(0);
noStroke();
textSize(25);

//text in the profiles
noStroke();
text('Laura Devantié', 180,230);
text('Nina Galsgaard',180,280);
text('Cecilie Christensen',180,330);
text('Amia Nielsen',180,380);
text('Kim Svenson',180,430);
text('Chili Young',180,480);
text('Nanna Hansen',180,530);
text('Mira Rasmussen',180,580);
text('Alan Johansen',180,630);

//text at the top bar of leaderboard
fill(254, 199, 255);
text('Friends',120,180);
text('Webs',500,180);
text('Level',650,180);

//text at the top bar of overview
fill(254, 199, 255);
text('Level overview',1010,180);


//Text for top
push ();
textSize(50);
fill(175, 97, 176);
textStyle(BOLD);
text('L E A D E R B O A R D', 100,100);
pop ();

//Level overview: names
fill(108, 107, 108   );
noStroke();
text('God',1130,230);
text('Legend',1130,280);
text('Hero',1130,330);
text('Knight',1130,380);
text('Slayer',1130,430);
text('Scout',1130,480);
text('Fighter',1130,530);
text('Novice',1130,580);
text('Beginner',1130,630);

//Level overview: ranks
fill(108, 107, 108);
noStroke();
text('🕸️ 90-100',965,230);
text('🕸️ 80-89',965,280);
text('🕸️ 70-79',965,330);
text('🕸️ 60-69',965,380);
text('🕸️ 50-59',965,430);
text('🕸️ 40-49',965,480);
text('🕸️ 30-39',965,530);
text('🕸️ 20-29',965,580);
text('🕸️ 10-19',965,630);


//Points
fill(0);
noStroke();
text('🕸️ 80',500,230);//Lauras Points
text('🕸️ 77',500,280);//Ninas Points
text('🕸️ 75',500,330);//Cecilies Points
text('🕸️ 69',500,380);//Amias Points
text('🕸️ 57',500,430);//Kims Points
text('🕸️ 55',500,480);//Chilis Points
text('🕸️ 50',500,530);//Ians Points
text('🕸️ 43',500,580);//Miras Points
text('🕸️ 42',500,630);//Alans Points

//Levels
fill(0);
noStroke();
text('Legend',650,230);//Lauras Level
text('Hero',650,280);//Ninas Level
text('Hero',650,330);//Cecilies Level
text('Knight',650,380);//Amias Level
text('Slayer',650,430);//Kims Level
text('Slayer',650,480);//Chilis Level
text('Slayer',650,530);//Ians Level
text('Scout',650,580);//Miras Level
text('Scout',650,630);//Alans Level


//Strokes between categories
stroke(0);
strokeWeight(1);
beginShape(LINES);
vertex(450, 151);
vertex(450, 650);
vertex(630, 151);
vertex(630, 650);
endShape();

//Stroke level overview
stroke(108, 107, 108);
strokeWeight(1);
beginShape(LINES);
vertex(1100, 200);
vertex(1100, 650);
endShape();

//Circle for ranking
noStroke();
fill(230, 184, 0);
circle(80,210,15);
fill(217, 217, 217);
circle(80,265,15);
fill(204, 122, 0);
circle(80,320,15);

//number for ranking
fill(0);
noStroke();
text('1',73,217);
text('2',73,272);
text('3',73,327);
text('4',73,372);
text('5',73,420);
text('6',73,472);
text('7',73,525);
text('8',73,575);
text('9',73,630);


}
