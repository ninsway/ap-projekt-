var forrest;
let img;
let img1;
let img2;

//preloading the json-file
function preload (){
forrest = loadJSON("words.json")
}

function setup(){
  createCanvas(600,600);
  noLoop();

//loading our images for the small trees
img = loadImage('assets/tree.png');
img1 = loadImage('assets/tree1.png');
img2 = loadImage('assets/tree2.png');
}

function draw(){
  background(16, 204, 247);
  //the sun
  push();
  noStroke();
  fill(245,187,87);
ellipse(40,40,80,80);
pop();

//cloud
push();
noStroke();
fill(255);
ellipse(30,70,50,50);
ellipse(55,55,50,50);
ellipse(80,60,40,40);
ellipse(90,70,45,45);
ellipse(60,80,50,50);
pop();

push();
textSize(12);
fill(179, 187, 182);
text('press for forrest',18,70);
pop();

  //the three small trees
  image(img, 340, 390, img.width / 4, img.height / 4);
  image(img1, 500, 380, img1.width / 4, img1.height / 4);
  image(img2, 10, 380, img2.width / 5, img2.height / 5);

push();
translate(width/2,height-20);
branch(0);
strokeWeight(0);
pop();

//drawing the grass
push();

fill(37, 146, 73);
noStroke();
square(0,580,800);

pop();
}

function branch(depth){
  if (depth < 7) {

// draws a line/branch going up
line(0,0,0,-height/10);

    push();
    rotate(radians(90));
    text(forrest.gump[depth],0,0);
    pop();
    {
      // move the space upwards
      translate(0,-height/10);
      // random wiggle
      rotate(random(-0.05,0.05));

      // more branching
      if (random(1.0) < 0.6){
        // rotate to the right
        rotate(0.3);
        // scale down
        scale(0.8);

        push();
        //another branch
        branch(depth + 1);
        pop();

        //rotate back to the left
        rotate(-0.6);

        push();
        // start another new branch
        branch(depth + 1);
        pop();


     }
      else { // no branch - continue at the same depth (same tree)
        branch(depth);
      }
    }
  }
}

//drawing a new tree when you press the screen
function mouseReleased(){
  redraw();
}
