# Forrest Gump: e-lit 

<img align="center" src="miniex_8/forrestgump.png" width="60%">

**Concept:**

This week I worked with Laura on making our miniex_8. We decided to do a Forrest Gump-themed program, and implemented this in multiple ways. Firstly we chose to use the dialogue transcript from the movie as the text in the JSON file. This appears when the tree in our program is drawn, it consists mainly of the quotes from the movie, combined with thin strokes, to make the tree shape more recognizable. It turned out to be a bit abstract, but we hope that it is possible to see the resemblance to a tree. Secondly, we added images of several smaller trees in the background, to make it show up as a forest – and suit the theme. 

I really enjoyed working with Laura, even though I was skeptical about coding in collaboration. Mainly because my personal coding progress often gets rather frustrating, but surprisingly it worked out really nicely to always have a second opinion and the fact that whenever we faced a challenge, we had two mindsets to problem solve together. I also believe my work with Laura was way more productive than when I work on my own, since I am constantly accountable when working with another person. 

**Technically:**

For this miniex, we wanted to learn how to incorporate JSON in p5.js. The idea behind JSON is really interesting, but when using it while coding we were tested numerous times. Calling the variable, and preloading the file was not a big deal, but actually making it appear on canvas was a struggle during our work. Now that it is done, the code for JSON being drawn looks simple, and hopefully next time it will get easier as well: 

`text(forrest.gump[depth],0,0);`

The code for drawing a tree with switching branches every time you press the screen, is one we borrowed from p5.js, which I will link down below. So after fully understanding this code, we worked with editing the branches and replacing them with words (from the JSON file). After that, we created some context to the tree, uploading images for the forest and adding elements to the sky. 

**Context:** 

I found it difficult to properly understand the material for this week, but if I were to put the program into context, I like what Winnie spoke about regarding semiotics. I believe it is highly relevant in connection to the way we built the program, making the text work as branches in order to simulate a tree. Winnie defined semantics to be: “the relations of signs to their context”, and syntactics: “formal structure”. In order to make the text symbolize the shape of a tree, the context plays a large role. The fact that there is a blue background, and a green part at the bottom lets the user imagine a landscape. Every time the tree is drawn, it starts with a vertical single line, and then random branches moving in random directions. All of these signs relate to each other and in context they work as a language, expressing a symbol we understand as a tree / a forest. 



[RUNME](https://glcdn.githack.com/ninsway/ap-projekt-/raw/master/miniex_8/miniex_8/index.html)

[Inspiration-code: The tree](https://editor.p5js.org/mtchl/sketches/ryyW2U5Fx)