# Flowcharts

### Individual
Try the game first: [here](https://cdn.staticaly.com/gl/ninsway/ap-projekt-/raw/master/miniex_6/miniex_6/index.html)

Readme for the game: [miniex_6](https://gitlab.com/ninsway/ap-projekt-/tree/master/miniex_6)

We were asked to choose our most complex miniex and create a flowchart to present the program. I decided to work with my miniex_6, where I created a game with class-oriented coding. 
For the flowchart I focus on the user experience when trying out the game and less on the technical aspects of the code. That means you start out detecting if the mouse is moving, and if it is, the cloud follows the movement. Then there are two events which can occur; either the cloud hits a lightning or a raindrop. If you hit a lightning, you meet an area with the cloud which you can not pass through (a barrier). If you hit a raindrop, the image of the cloud with the tongue out will appear and the raindrop is removed. You can continue this step until all the raindrops are gone and the game is won. 


<img src="miniex_10/flowcharts/Flowchart_me.png" width="60%">


### Group CLAN
**Walled Garden**

Yellow flowchart. For the idea about a walled garden, we work with data capturing and the concept of a secret locked garden filled with inaccessible data. You can only unlock the door by accepting terms and conditions. This idea was nice but not quite what we were interested in working with.

**Ranked Donation** 

Purple flowchart. This is the idea we will most likely be using for our final exam project. We got inspired by a feature on Facebook where you are offered to start a fundraising for a self-chosen charity on your birthday. This is a way to show your friends and family what a good person you are, and we wanted to do a critical program of this concept. Therefore we created a ranking system where you get points every time you donate to charity. All the data regarding your current charity points are public, and everyone can keep track of how often you donate = how good of a person you are. 
The flowchart starts out with a reminder, to remember to donate today. Therefrom you can choose to donate, how much, and receive the points. 

There are some technical issues in creating a functioning program with “effects” whenever a donation is happening. I hope we will find a smart solution for this. 



<img src="miniex_10/flowcharts/Flowchart_garden.png" width="40%">
<img src="miniex_10/flowcharts/Flowchart_donation.png" width="40%">

### Considerations 

When comparing my flowchart to the two we made in our group, it is clear that I was able to make mine way more detailed. I think this was possible because the program is already done and I know exactly what it looks like. The flowcharts in my group, are created based on ideas which we are not entirely sure about yet. When they are further developed I am sure we can create more accurate flowcharts as well. 

Algorithms somehow contain a row of instructions and rules regarding how to run a program. This is separated into describable parts in a flowchart and visualized in these differently shaped boxes. It makes the process easier to comprehend and I believe that it can be used in many societal contexts to explain complex things. As in politics or social constructions, the processes can often seem confusing to understand. Here flowcharts can help in the way they simplify and separate each decision and its consequences. 




