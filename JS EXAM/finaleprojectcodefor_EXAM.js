let lead, back, profile, upda, fbbut, insbut, snabut, twibut, tinbut, youbut, spobut; //button variables
let img1, img2, img3, img4, img5, img6, img7, img8, img9; //image avatars
let logo1, logo2, logo3, logo4, logo5, logo6, logo7; //logo
let weblinks; //links to sites
let fblike=12, inlike=7, snlike=9, twlike=5, tilike=5, yolike=6, splike=8; //variables for reloading page
let sublikes; //the total likes

function preload() { //loade image before page
  //load profile picture
  profile=loadImage('Avatars/AvatarProfile.png'); //Loading avatar on profile
  sora=loadImage('Avatars/SORA.png'); //loading logo

  //Load Avatars
  img1= loadImage('Avatars/Person1.png');//Laura
  img2= loadImage('Avatars/Person2.png');//Nina
  img3= loadImage('Avatars/Person3.png');//Cecilie
  img4= loadImage('Avatars/Person4.png');//Amia
  img6= loadImage('Avatars/Person6.png');//Kim
  img7= loadImage('Avatars/Person7.png');//Chili
  img8= loadImage('Avatars/Person8.png');//Ian
  img9= loadImage('Avatars/Person9.png');//Mira
  img5= loadImage('Avatars/Person5.png');//Alan

  //Load Logos
  logo1= loadImage('Logos/Facebook-logo.png');
  logo2= loadImage('Logos/Instagram-logo.png');
  logo3= loadImage('Logos/Snapchat-logo.png');
  logo4= loadImage('Logos/Spotify-logo.png');
  logo5= loadImage('Logos/Tinder-logo.png');
  logo6= loadImage('Logos/Twitter-logo.png');
  logo7= loadImage('Logos/YouTube-logo.png');

  //Array for links to different social media
  weblinks=['https://www.facebook.com/','https://www.instagram.com/','https://www.snapchat.com/l/da-dk/','https://twitter.com/','https://tinder.com/','https://www.youtube.com/','https://open.spotify.com/browse/featured']
}

//"Update" your scoreboard
function uplikes(){ //buttons are hidden so you can see the numbers
lead.hide(); //invisible buttons (links)
  upda.hide();
  spobut.hide();
  youbut.hide();
  tinbut.hide();
  twibut.hide();
  snabut.hide();
  insbut.hide();
  fbbut.hide();

//giving the variables new numbers, which differ between -1 and 2
  fblike=fblike+floor(random(-1,2));
  inlike=inlike+floor(random(-1,2));
  snlike=snlike+floor(random(-1,2));
  twlike=twlike+floor(random(-1,2));
  tilike=tilike+floor(random(-1,2));
  yolike=yolike+floor(random(-1,2));
  splike=splike+floor(random(-1,2));

  redraw(); //function redraw (a p5 function)
}

function setup(){
  createCanvas(1400,770);  //to make it all fit the computer screen
}

function draw(){
  noLoop();// does not draw the functions again (function draw does not loop now)
  background(153, 204, 255);

  //Show SoRa-logo (getting called)
  image(sora,620,90,220,500);

  //Button for going back (styling for the button)
  back=createButton('🔙');
  back.position(110,660);
  back.style('padding','2px 70px');
  back.style('background','#0086b3');
  back.style('font-size','25px');
  back.style('color', 'white');
  back.mousePressed(goback)
  back.hide(); //hidden on the first page

  //Button for leaderboard (styling)
  lead=createButton('L E A D E R B O A R D');
  lead.position(230,600);
  lead.size(180,50);
  lead.style('background','#FE94FF');
  lead.style('font-size', '13px');
  lead.style('color', 'black');
  lead.mousePressed(leaderboard);

  //Button for update (styling)
  upda=createButton('U P D A T E');
  upda.position(420,600);
  upda.size(180,50);
  upda.style('background','#FE94FF');
  upda.style('font-size', '13px');
  upda.style('color', 'black');
  upda.mousePressed(uplikes);

  //Direct to Facebook
  fbbut=createButton(' '); //invisible button - The button is called - it is a variable even without let
  fbbut.position(820,110);
  fbbut.style('font-size','24px');
  fbbut.style('border','none'); //no border
  fbbut.style('background-color','#00E6AC00');
  fbbut.style('padding','20px 200px')
  fbbut.style('cursor','pointer') //The mouse changes to be a hand, when over the button (pressable)
  fbbut.mousePressed(face);

  //Direct to Instagram
  insbut=createButton(' ');
  insbut.position(820,195);
  insbut.style('font-size','24px');
  insbut.style('border','none');
  insbut.style('background-color','#00E6AC00');
  insbut.style('padding','20px 200px')
  insbut.style('cursor','pointer')
  insbut.mousePressed(insta);

  //Direct to Snapchat
  snabut=createButton(' ');
  snabut.position(820,265);
  snabut.style('font-size','24px');
  snabut.style('border','none');
  snabut.style('background-color','#00E6AC00');
  snabut.style('padding','20px 200px')
  snabut.style('cursor','pointer')
  snabut.mousePressed(snap);

  //Direct to Twitter
  twibut=createButton(' ');
  twibut.position(820,320);
  twibut.style('font-size','24px');
  twibut.style('border','none');
  twibut.style('background-color','#00E6AC00');
  twibut.style('padding','20px 200px')
  twibut.style('cursor','pointer')
  twibut.mousePressed(twitter);

  //Direct to Tinder
  tinbut=createButton(' ');
  tinbut.position(820,390);
  tinbut.style('font-size','24px');
  tinbut.style('border','none');
  tinbut.style('background-color','#00E6AC00');
  tinbut.style('padding','20px 200px')
  tinbut.style('cursor','pointer')
  tinbut.mousePressed(tinder);

  //Direct to YouTube
  youbut=createButton(' ');
  youbut.position(820,460);
  youbut.style('font-size','24px');
  youbut.style('border','none');
  youbut.style('background-color','#00E6AC00');
  youbut.style('padding','20px 200px')
  youbut.style('cursor','pointer')
  youbut.mousePressed(youtube);

  //Direct to Spotify
  spobut=createButton(' ');
  spobut.position(820,530);
  spobut.style('font-size','24px');
  spobut.style('border','none');
  spobut.style('background-color','#00E6AC00');
  spobut.style('padding','20px 200px')
  spobut.style('cursor','pointer')
  spobut.mousePressed(spotify);



  //the square around the profile - styling
  fill(254, 199, 255);
  stroke(128,0,128);
  rect(200,100,450,490,20);

  //square around the profile picture - styling
  fill(255);
  noStroke();
  rect(325,120,200,200,20);

  //Show profile picture (being called)
  image(profile, 340, 130, 170, 170);

  //social media boxes - styling
  fill(254, 199, 255);
  stroke(128,0,128);
  rect(800,100,450,70,20);
  rect(800,170,450,70,20);
  rect(800,240,450,70,20);
  rect(800,310,450,70,20);
  rect(800,380,450,70,20);
  rect(800,450,450,70,20);
  rect(800,520,450,70,20);

  //Current rating box
  rect(200,40,1050,50,20);
  fill(175, 97, 176);
  textSize(30);
  textStyle(BOLD);
  sublikes=fblike+inlike+snlike+twlike+tilike+yolike+splike; // Calculator - sublikes is the total of all the seperate likes
  text('C  U  R  R  E  N  T     R  A  T  I  N  G:    🕸️ '+sublikes,420,75); //variable called

  //text in profile - styling
  textSize(15);
  noStroke();
  text('Y O U R   P R O F I L E',345,340)
  fill(0);
  text('NAME:',250,385);
  text('AGE:',250,415);
  text('OCCUPATION:',250,445);
  text('TIME ONLINE:',250,475);
  text('STATUS:',250,505);
  text('MOTTO:',250,535);
  textStyle(NORMAL);
  noStroke();
  text('Nanna Hansen',380,385);
  text('23',380,415);
  text('Studying at Oxford University',380,445);
  text('4 hours', 380,475);
  text('In a relationship with Ian',380,505);
  text('"Live life bold"',380,535);

  //logos
  image(logo1, 825, 115, 35, 35);
  image(logo2, 825, 185, 38, 38);
  image(logo3, 822, 255, 42, 40);
  image(logo4, 820, 530, 50, 50);
  image(logo5, 822, 398, 35, 35);
  image(logo6, 822, 325, 40, 40);
  image(logo7, 820, 462, 50, 50);

  //Text in social media boxes
  fill(0);
  textSize(25);
  text('Facebook',880,145);
  text('Instagram',880,215);
  text('Snapchat',880,285);
  text('Twitter', 880,355);
  text('Tinder', 880,425);
  text('Youtube', 880,495);
  text('Spotify',880,565);

  //Text for points, including the seperate variable for points
  text('🕸️ '+fblike,1150,145); // checks its value in variable (points, placement, placement)
  text('🕸️ '+inlike,1150,215);
  text('🕸️ '+snlike,1150,285);
  text('🕸️ '+twlike, 1150,355);
  text('🕸️ '+tilike, 1150,425);
  text('🕸️ '+yolike, 1150,495);
  text('🕸️ '+splike,1150,565);
}

//When leaderboard-button is pressed
function leaderboard(){

  //Show Back-button
  back.show();// get back to the first page

  //Hide all other buttons from the front page
  lead.hide();
  upda.hide();
  spobut.hide();
  youbut.hide();
  tinbut.hide();
  twibut.hide();
  snabut.hide();
  insbut.hide();
  fbbut.hide();

  //Background - the rect covering the first page
  fill(153, 204, 255);
  stroke(0,51,102);
  rect(0,0,width,height);


  //Text for top - styling
  push ();
  textSize(50);
  fill(175, 97, 176);
  textStyle(BOLD);
  text('L E A D E R B O A R D', 100,100);
  pop ();

  //Logo
  image(sora,825,160,260,500);

  //rect with the dark blue color
  fill(0, 134, 179);
  rect(100,150,750,50,20);

  //Level overview - styling
  stroke(108, 107, 108);
  fill(0, 134, 179,200);
  rect(1050,150,300,50,20);

  //text at the top bar of leaderboard
  fill(254, 199, 255);
  textSize(24);
  text('Friends',120,185);
  text('Webs',500,185);
  text('Level',650,185);

  //text at the top bar of overview
  fill(254, 199, 255, 200);
  text('Level overview',1110,185);

  //Profile pictures (called)
  image(img1, 115, 205, 37, 37);//Laura
  image(img2, 115, 255, 37, 37);//Nina
  image(img3, 115, 305, 37, 37);//Cecilie
  image(img4, 115, 355, 37, 37);//Amia
  image(img6, 115, 405, 37, 37);//Kim
  image(img7, 115, 455, 37, 37);//Chili
  image(profile, 115, 505, 37, 37);//Nanna
  image(img9, 115, 555, 37, 37);//Mira
  image(img5, 115, 605, 37, 37);//Alan

  //text in the profiles
  fill(0);
  noStroke();
  textSize(25);
  text('Laura Devantié', 180,230);
  text('Nina Galsgaard',180,280);
  text('Cecilie Christensen',180,330);
  text('Amia Nielsen',180,380);
  text('Kim Svenson',180,430);
  text('Chili Young',180,480);
  text('Nanna Hansen',180,530);
  text('Mira Rasmussen',180,580);
  text('Alan Johansen',180,630);

  //Points
  text('🕸️ 80',500,230);//Lauras Points
  text('🕸️ 77',500,280);//Ninas Points
  text('🕸️ 75',500,330);//Cecilies Points
  text('🕸️ 69',500,380);//Amias Points
  text('🕸️ 64',500,430);//Kims Points
  text('🕸️ 60',500,480);//Chilis Points
  sublikes=fblike+inlike+snlike+twlike+tilike+yolike+splike;//new page= check the points again (synchronizes)
  text('🕸️ '+sublikes,500,530);//Nannas Points
  text('🕸️ 47',500,580);//Miras Points
  text('🕸️ 41',500,630);//Alans Points


  //Levels - styling
  text('Legend',650,230);//Lauras Level
  text('Hero',650,280);//Ninas Level
  text('Hero',650,330);//Cecilies Level
  text('Knight',650,380);//Amias Level
  text('Knight',650,430);//Kims Level
  text('Knight',650,480);//Chilis Level
  text('Slayer',650,530);//Nannas Level
  text('Scout',650,580);//Miras Level
  text('Scout',650,630);//Alans Level

  //Circle for ranking - Gold, silver, bronze
  noStroke();
  fill(230, 184, 0);
  circle(80,210,15);
  fill(217, 217, 217);
  circle(80,265,15);
  fill(204, 122, 0);
  circle(80,320,15);

  //number for ranking - styling
  fill(0);
  text('1',73,217);
  text('2',73,272);
  text('3',73,327);
  text('4',73,372);
  text('5',73,420);
  text('6',73,472);
  text('7',73,525);
  text('8',73,575);
  text('9',73,630);

  //leaderboard rects - styling
  noFill();
  stroke(0,51,102);
  rect(100,150,750,100,20); //the square around the profile
  rect(100,150,750,150,20); //the square around the profile
  rect(100,150,750,200,20); //the square around the profile
  rect(100,150,750,250,20); //the square around the profile
  rect(100,150,750,300,20); //the square around the profile
  rect(100,150,750,350,20); //the square around the profile
  rect(100,150,750,400,20); //the square around the profile
  rect(100,150,750,450,20); //the square around the profile
  rect(100,150,750,500,20); //the square around the profile


  //Strokes/lines between categories (lines inbetween) - styling
  stroke(0,51,102);
  strokeWeight(1);
  beginShape(LINES);
  vertex(450, 151);
  vertex(450, 650);
  vertex(630, 151);
  vertex(630, 650);
  endShape();

  //Level overview rects - styling
  stroke(108, 107, 108);
  rect(1050,150,300,100,20); //the square around the profile
  rect(1050,150,300,150,20); //the square around the profile
  rect(1050,150,300,200,20); //the square around the profile
  rect(1050,150,300,250,20); //the square around the profile
  rect(1050,150,300,300,20); //the square around the profile
  rect(1050,150,300,350,20); //the square around the profile
  rect(1050,150,300,400,20); //the square around the profile
  rect(1050,150,300,450,20); //the square around the profile
  rect(1050,150,300,500,20); //the square around the profile

  //Level overview: names
  fill(108, 107, 108);
  noStroke();
  text('God',1230,230);
  text('Legend',1230,280);
  text('Hero',1230,330);
  text('Knight',1230,380);
  text('Slayer',1230,430);
  text('Scout',1230,480);
  text('Fighter',1230,530);
  text('Novice',1230,580);
  text('Beginner',1230,630);

  //Level overview: ranks (explaination box)
  text('🕸️ 90-100',1065,230); //text
  text('🕸️ 80-89',1065,280);
  text('🕸️ 70-79',1065,330);
  text('🕸️ 60-69',1065,380);
  text('🕸️ 50-59',1065,430);
  text('🕸️ 40-49',1065,480);
  text('🕸️ 30-39',1065,530);
  text('🕸️ 20-29',1065,580);
  text('🕸️ 10-19',1065,630);


  //Stroke level overview (lines)
  strokeWeight(1);
  beginShape(LINES);
  vertex(1200, 200);
  vertex(1200, 650);
  endShape();
}


//Functions to open the web-pages

//Open facebook
function face(){
  window.open(weblinks[0]);
}

//open instagram
function insta(){
  window.open(weblinks[1]);
}

//open snapchat
function snap(){
  window.open(weblinks[2]);
}

//open snapchat
function twitter(){
  window.open(weblinks[3]);
}

//open tinder
function tinder(){
  window.open(weblinks[4]);
}

//open youtube
function youtube(){
  window.open(weblinks[5]);
}

//open spotify
function spotify(){
  window.open(weblinks[6]);
}

//Go to main page again  (looking like mainpage)
function goback(){

  //Hide back-button
  back.hide();

  //Show buttons again (transparent buttons to webpages)
  lead.show();
  upda.show();
  spobut.show();
  youbut.show();
  tinbut.show();
  twibut.show();
  snabut.show();
  insbut.show();
  fbbut.show();

  //"Background" //rect that covers the leaderboard page
  fill(153, 204, 255);
  stroke(0);
  rect(0,0,width,height);

  //Logo (pic being called)
  image(sora,620,90,220,500);


  //the square around the profile - styling
  fill(254, 199, 255);
  stroke(128,0,128);
  rect(200,100,450,490,20);

  //square around the profile picture - styling
  fill(255);
  noStroke();
  rect(325,120,200,200,20);

  //Show profile picture (pic being called)
  image(profile, 340, 130, 170, 170);

  //social media boxes - styling
  fill(254, 199, 255);
  stroke(128,0,128);
  rect(800,100,450,70,20);
  rect(800,170,450,70,20);
  rect(800,240,450,70,20);
  rect(800,310,450,70,20);
  rect(800,380,450,70,20);
  rect(800,450,450,70,20);
  rect(800,520,450,70,20);

  //Current rating box
  rect(200,40,1050,50,20);
  fill(175, 97, 176);
  textSize(30);
  textStyle(BOLD);
  sublikes=fblike+inlike+snlike+twlike+tilike+yolike+splike; //new page= check the points again (synchronizes)
  text('C  U  R  R  E  N  T     R  A  T  I  N  G:    🕸️ '+sublikes,420,75); //Nannas sum of points

  //text in profile - styling
  textSize(15);
  noStroke();
  text('Y O U R   P R O F I L E',345,340)
  fill(0);
  text('NAME:',250,385);
  text('AGE:',250,415);
  text('OCCUPATION:',250,445);
  text('TIME ONLINE:',250,475);
  text('STATUS:',250,505);
  text('MOTTO:',250,535);
  textStyle(NORMAL);
  noStroke();
  text('Nanna Hansen',380,385);
  text('23',380,415);
  text('Studying at Oxford University',380,445);
  text('4 hours', 380,475);
  text('In a relationship with Ian',380,505);
  text('"Live life bold"',380,535);

  //logos (calling the logo-variables)
  image(logo1, 825, 115, 35, 35);
  image(logo2, 825, 185, 38, 38);
  image(logo3, 822, 255, 42, 40);
  image(logo4, 820, 530, 50, 50);
  image(logo5, 822, 398, 35, 35);
  image(logo6, 822, 325, 40, 40);
  image(logo7, 820, 462, 50, 50);

  //Text in social media boxes - styling
  fill(0);
  textSize(25);
  text('Facebook',880,145);
  text('Instagram',880,215);
  text('Snapchat',880,285);
  text('Twitter', 880,355);
  text('Tinder', 880,425);
  text('Youtube', 880,495);
  text('Spotify',880,565);

  //Text for points
  text('🕸️ '+fblike,1150,145); // (Points for facebook, placement)
  text('🕸️ '+inlike,1150,215);
  text('🕸️ '+snlike,1150,285);
  text('🕸️ '+twlike, 1150,355);
  text('🕸️ '+tilike, 1150,425);
  text('🕸️ '+yolike, 1150,495);
  text('🕸️ '+splike,1150,565);
}
