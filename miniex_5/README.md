<img src="miniex_5/whitediamonds.png" width="40%"> 
<img src="miniex_5/girldiamonds.png" width="40%">

[Link to miniex_5](https://cdn.staticaly.com/gl/ninsway/ap-projekt-/raw/master/miniex_5/miniex_5/index.html)

[Link to miniex_1](https://cdn.staticaly.com/gl/ninsway/ap-projekt-/raw/master/miniex_1/empty-example/index.html)

**Technically**

For my first mini exercise I had designed three rotating cones, and since working with WEBGL was very new for me, my options for designing interaction were limited. I worked with the mouseMoved function to regulate the color-scale in the cones, randomly switching from blue shades to pink to purple. I tried working with an if-statement for the first time, in the mouseMoved function which I also included in my miniex_5. 

In this week’s program I chose to challenge myself with some more advanced if-syntaxes, using both “if else” and “else” in order to make the color switch of the diamonds work to my liking. I am sure there is a simpler way to achieve this, but I struggled a bit with regulating the color-shift of each button. In this fifth mini exercise I also used my new knowledge regarding working outside of the canvas, with HTML elements such as buttons which I used here. In my first mini_ex I had problems with positioning the diamonds and controlling their rotation, because I did not understand exactly what the syntax ‘translate’ does and how it works. This I handled by using push () and pop () – other syntaxes I have learnt since last time. 
All of this made my fifth mini_ex more technically more advanced and intentional in connection to what I actually hoped to create, than my first mini_ex. 

**Conceptually** 

Since my first mini_ex’s purpose was to explore syntaxes and slowly understand coding, it did not have any further conceptual meaning. I wanted to implement this in my new mini_ex by working with the topic of gender that we touched upon in class 04. We discussed how buttons work as an invitation for the user to interact with/ a call for action. Buttons can also change the way you think about things, by only adding limited options. This is the point I worked with, by creating an option for parents to choose “it’s a boy!”, “it’s a girl!” or “born without gender”.  The program reacts to which button you press, by changing colors on the floating diamonds. Pressing boy makes them turn blue, girl turns them pink and no-gender turns them black. I chose black to symbolize how it is something unknown and yet to be revealed. A bit like when you have yet to unlock a character in a videogame and all you can see is the black silhouette. Or when a user in Facebook have not added a profile picture yet and you just see the outline of a person. 
