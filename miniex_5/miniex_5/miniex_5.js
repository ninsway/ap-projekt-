var button;
let value;
let diamondsboy;
let diamondsgirl;
let diamondsnogender;

function setup() {
  createCanvas(700, 500,WEBGL);

//boy
button = createButton("it's a boy!");
button.mousePressed(diamondsBoy);
button.style('font-size', '30px');
button.position(180,510);

//girl
button = createButton("it's a girl!");
button.mousePressed(diamondsGirl);
button.style('font-size', '30px');
button.position(10,510);

//no-gender
button = createButton("born without gender");
button.mousePressed(diamondsGender);
button.style('font-size', '30px');
button.position(360,510);
}

//the functions
function diamondsBoy(){
value = 100;
}

function diamondsGirl(){
value = 300;
}

function diamondsGender(){
value = 0;
}

//diamonds
function draw() {
      background(225, 225, 225);
push ();
    rotateX(frameCount * 0.008);
  rotateZ(frameCount * 0.003);
    if(value == undefined) {
        fill(255,255,255);
    }
    else if(value == 0) {
        fill(0,0,0);
    }
    else {
        fill(value, 160, 190);
    }
    translate(0,70);
    cone(60, 140);
pop ();

push ();
    rotateX(frameCount * 0.001);
  rotateZ(frameCount * 0.005);
    if(value == undefined) {
        fill(255,255,255);
    }
    else if(value == 0) {
        fill(0,0,0);
    }
    else {
        fill(value, 80, 220);
    }
    translate(-200,40);
    cone(40, 80);
pop ();

push ();
    if(value == undefined) {
        fill(255,255,255);
    }
    else if(value == 0) {
        fill(0,0,0);
    }
    else {
        fill(value, 160, 220);
    }
    rotateX(frameCount * 0.001);
  rotateZ(frameCount * 0.009);
    translate(100,100);
    cone(40, 70);
pop ();
}
