# **Cloudburst**

![Screenshot](miniex_6/cloudyy.png)

**Concept:**

For this week’s mini exercise I programmed a simple game called Cloudburst. I chose to keep it plain, so I could focus on the details and making it successfully work almost how I intended. In the game, you control a sprite shaped like a cloud, which follows your cursor on the screen. The goal is to collide into all the raindrops, in order to “eat them” or “pick them up”. By adding obstacles that the cloud can’t pass through, I hoped that it would make the game slightly harder. 

Once all raindrops have been removed from the screen, the user has won the game and a text appears on the screen saying “you won!”. I got inspiration for the game from one of the examples in p5.js play library called “Collisions – between group, events functions”. 

**Technically:**

I chose to create my own objects in this program, using two different ways. For the sprite(cloud), raindrops and lightning, I added images to my ‘assets’ folder and used the drawSprites(); syntax from the p5.play library. It was important to me that the game would change every time the page is refreshed, so the user approached a new challenge each time. Therefore I used random placement in a loop (line 20-31 in my code).  

The other way I added an object, was with the class-based approach, which I struggled a bit with. I wanted to add a guide to my game on screen, but with the randomly placed lightning, they would often clash into each other. Therefore I used the class-syntax in order to draw a cloud, that could work as a background for the rules/guide. Since each white circle needed their own individual x-position and y-position to imitate the shape of a cloud, the only thing they had in common was the radius (size). So I drew a class called “Cloud” where the constructor included this.r = the radius. This way I could change a single number and affect all of circles at the same time, which was effective when I decided what size of my cloud was optimal. On line 93 in my code I added the attributes in show(), including the noStroke (); syntax, fill, and several ellipsis. This classifies what a cloud consists of, and what it looks like, as a template. If I wanted a cloud that is floating, raining etc., I could have added move () and given it attributes that define how it moves/what it does. 

**Context:** 

During class 06, Winnie gave multiple examples of how “object oriented programming” can be defined. In connection to my program this week, there are a few quotes that I find particularily relevant. When understanding the concept of objects, the quote: “A program execution is regarded as a physical model, simulating the behavior of either a real or imaginary part of the world” (Madsen et al, 1993, p.16) partially explains the thought behind my game this week. It is a program, simulating objects we know from the real world in cartoon-style appearance, but with an imaginary behavior. By imaginary behavior I mean how the raindrops are static and the cloud is an object that the user (human) can control, which unfortunately does not work that way in the real world. In object oriented programming, you have the possibility to play around with objects you recognize from your everyday life and give them new attributes both in shape and behavior. When using these objects in coding, a certain level of abstraction can be necessary. Winnie mentioned a definition of  abstraction as: “a generalization of an object which includes only those details necessary to define it and to differentiate it from other objects" (Lee 2013, p. 32)

In the class I coded (the cloud), this quote is very true. The combination of white ellipsis symbolize a generalization of a real life cloud, and with the round shapes and white somewhat transparent shapes it is possible to recognize what they symbolize. 


[Link to my game](https://cdn.staticaly.com/gl/ninsway/ap-projekt-/raw/master/miniex_6/miniex_6/index.html)
