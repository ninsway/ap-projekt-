var obstacles;
var collectibles;
var asterisk;
var j;
var cloud;
var text;

function setup() {
createCanvas(windowWidth, windowHeight);
obstacles = new Group ();
collectibles = new Group();
cloud = new Cloud();

//the cloud sprite
asterisk = createSprite(400, 200);
  asterisk.addAnimation('normal', 'assets/hapicloud0001.png','assets/hapicloud0001.png');
  asterisk.addAnimation('stretch', 'assets/hihicloud0001.png','assets/hihicloud0004.png');
  
//random placement of lightning
  for(var i=0; i<6; i++) {
     var box = createSprite(random(0, width), random(0, height));
     box.addAnimation('normal', 'assets/lightyy.png');
     obstacles.add(box);
   }

//random placement of raindrops
  for(j=0; j<15; j++) {
    var dot = createSprite(random(0, width), random(0, height));
    dot.addAnimation('drippyy/drippy.png', 'drippyy/drippy.png');
    collectibles.add(dot);
  }
}

function draw() {
background(32, 185, 220);

//how far the cloud drags behind the cursor
asterisk.velocity.x = (mouseX-asterisk.position.x)/10;
asterisk.velocity.y = (mouseY-asterisk.position.y)/10;

asterisk.collide(obstacles);
asterisk.overlap(collectibles, collect);

if(asterisk.getAnimationLabel() == 'stretch' && asterisk.animation.getFrame() == asterisk.animation.getLastFrame()) {
asterisk.changeAnimation('normal');
}

drawSprites();

//an if-sentence that calls for the "you won" text when all raindrops are taken
push();
  if(j==0){
    textSize(40);
    textStyle(BOLD);
  text('you won!', windowWidth/2-100, windowHeight/2);
  fill(0, 0, 0);
  }
pop();

//showing my class
cloud.show();

//the rules
fill(0, 0, 0);
textSize(15);
text('eat raindrops', 20, 70);
text('avoid lightning', 20, 90);
text('refresh page if impossible', 20, 110);
textStyle(BOLD);
text('how to play:', 20, 50);
}

//the function for collecting raindrops
function collect(collector, collected) {

//the facial expression of the cloud
  collector.changeAnimation('stretch');
  collector.animation.rewind();
  collected.remove();

//this helps count how many raindrops there are left
  j=j-1;
}

//my class for the cloud behind the rules
//this.r decides the size of the cloud (radius of ellipses)
class Cloud {
constructor() {
  this.r = 90;
}

//attributes
show() {
noStroke();
fill(255,255,255,200);
ellipse(100, 100, this.r);
ellipse(150, 100, this.r);
ellipse(60, 100, this.r);
ellipse(30, 100, this.r);
ellipse(180, 100, this.r);
ellipse(130, 80, this.r);
ellipse(100, 80, this.r);
ellipse(70, 80, this.r);
ellipse(30, 80, this.r);
ellipse(50, 50, this.r);
ellipse(90, 50, this.r);
}
}
