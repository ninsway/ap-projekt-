![ScreenShot](miniex_2/mood.png)

https://cdn.staticaly.com/gl/ninsway/ap-projekt-/raw/master/miniex_2/første%20sketch/index.html

For the mini exercise of this week, I wanted to explore the purpose of using emojis. After reading the text “Modifying the universal” it caught my interest that people in modern society feel such a need for identifying themselves in emojis and create a connection to their visual design. Emoticons no longer serve a purpose of solely showing a feeling or an emotion, but they get designed in various genders and races in order to include and portray as many different types of people as possible. With my program, I have chosen to take emojis back to basic, to do what they do best. Show emotion. 

I chose purposely to avoid any sort of resemblance to skin color and gender. Since the yellow color in original emojis got criticized (p. 40, Modifying the universal), I went with an unrealistic grey color in the program. My code ended up being rather complex, since I became a bit too ambitious with the interaction I wanted to implement in this week’s program. I wanted to create four options/buttons for the user to press, with an individual reaction happening in the same space. 

I started out as usual, creating a canvas and by choosing my neutral non-offending grey background. Then I drew four options for the shapes of the mouth, by using a bunch of small ellipses. There is no further explanation for this, besides the fact I found them cute. In my code, I had to change the circles from working as shapes on the canvas, to be working as functions, which is why it might look a bit confusing upon reading the code. Then I drew the eyes, and circle to resemblance a head shape. After this, I added the text at the bottom to indicate that an action is possible. 

Now my struggle of the week started. I wanted to explore the syntax mouseIsPressed along with the new if-sentences I had watched in Daniel Schiffman’s video. I did this by writing that **if** the left mouse button is pressed and positioned at a certain area (over the specific mouth), then this mouth has to show up on the face at top. This is why I needed each mouth-shape to be a function. I named them: smile, mad, meh and o, and I was able to write it into my if-sentence. That if the mouse is pressed at a certain place, then a certain function (mouth-shape) supposed to show up at a specific coordinate (the face at top). I am not sure whether this was the most efficient way to do as I wanted, but at least it worked. 

Some people might argue that a smile is not enough to indicate a ‘mood’ or a feeling which was my purpose with this project. So if I had the time and skills to for example different eyes, that would have been a great way to develop the program. But once you add different hair-types and noses and move into the SIMS-world of modification, that is where it might get tricky and you possibly run into the same issues as Unicode. 

I learned a lot about placement of shapes in a canvas by working with small ellipses and I finally figured out how to use the consol to my own advantage. This helped me with tasks such as getting the right coordinates for placement of the mouth at the top-emoji. 

I hope you enjoy. 

