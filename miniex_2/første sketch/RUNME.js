function setup() {
  createCanvas(600, 600);
}

function draw() {
	  background(100);

	translate(200,100)

 //:) smile
noFill();
smile(0,0);

	//:/ face
meh(0,0);


	//:( face
mad(0,0);

		//:o face
o(0,0);

	//eyes o.o
ellipse(120, -50, 15, 15);
ellipse(100, -50, 15, 15);

//little eyes
fill(0,0,0);
ellipse(119, -50, 7, 7);
ellipse(101, -50, 7, 7);

//face circle
noFill();
ellipse(110, -40, 90, 90);

	//text
textSize(32);
fill(0, 0, 0);
text('^ choose your mood ^', -40,270);

	if (mouseIsPressed) {
    if (mouseButton === LEFT) {
			if(mouseX>214 && mouseX<277 && mouseY>142 && mouseY<162){
      smile(65,-75);

    }
  }

		if (mouseIsPressed) {
    if (mouseButton === LEFT) {
			if(mouseX>214 && mouseX<277 && mouseY>250 && mouseY<278){
      mad(66,-187);
 } } }

		if (mouseIsPressed) {
    if (mouseButton === LEFT) {
			if(mouseX>345 && mouseX<400 && mouseY>141 && mouseY<148){
      meh(-65,-70);
 } } }

		if (mouseIsPressed) {
    if (mouseButton === LEFT) {
			if(mouseX>360 && mouseX<400 && mouseY>251 && mouseY<290){
      o(-70,-190);
 } } }

	}

  print(mouseButton);
}



function smile(xpos,ypos) {
ellipse(xpos+ 20, ypos+46, 10, 10);
ellipse(xpos+30, ypos+50, 10, 10);
ellipse(xpos+40, ypos+54, 10, 10);
ellipse(xpos+50, ypos+54, 10, 10);
ellipse(xpos+60, ypos+50, 10, 10);
ellipse(xpos+70, ypos+46, 10, 10);

}


function mad(xpos,ypos) {
ellipse(xpos+20, ypos+170, 10, 10);
ellipse(xpos+30, ypos+166, 10, 10);
ellipse(xpos+40, ypos+162, 10, 10);
ellipse(xpos+50, ypos+162, 10, 10);
ellipse(xpos+60, ypos+166, 10, 10);
ellipse(xpos+70, ypos+170, 10, 10);
}

function meh(xpos,ypos) {
ellipse(xpos+150, ypos+46, 10, 10);
ellipse(xpos+160, ypos+46, 10, 10);
ellipse(xpos+170, ypos+46, 10, 10);
ellipse(xpos+180, ypos+46, 10, 10);
ellipse(xpos+190, ypos+46, 10, 10);
ellipse(xpos+200, ypos+46, 10, 10);
}

function o(xpos,ypos) {
ellipse(xpos+190, ypos+170, 10, 10);
ellipse(xpos+180, ypos+160, 10, 10);
ellipse(xpos+170, ypos+170, 10, 10);
ellipse(xpos+180, ypos+180, 10, 10);
}
